//
//  window_shopperTests.swift
//  window-shopperTests
//
//  Created by Jelo Alindogan on 11/12/2017.
//  Copyright © 2017 Emilio Angelo Alindogan. All rights reserved.
//

import XCTest

class window_shopperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testGetHours()  {
        XCTAssert(Wage.getHours(ForWage: 25.0, AndPrice: 100.0) == 4)
        XCTAssert(Wage.getHours(ForWage: 15.50, AndPrice: 250.53) == 17)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
