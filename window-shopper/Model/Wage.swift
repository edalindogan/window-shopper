//
//  Wage.swift
//  window-shopper
//
//  Created by Jelo Alindogan on 11/12/2017.
//  Copyright © 2017 Emilio Angelo Alindogan. All rights reserved.
//

import Foundation

class Wage {
    
    class func getHours(ForWage wage: Double, AndPrice price: Double) -> Int {
        return Int(ceil(price / wage))
    }
}
