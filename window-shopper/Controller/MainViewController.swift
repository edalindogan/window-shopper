//
//  ViewController.swift
//  window-shopper
//
//  Created by Jelo Alindogan on 10/12/2017.
//  Copyright © 2017 Emilio Angelo Alindogan. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var wageTxt: CustomTextField!
    @IBOutlet weak var priceTxt: CustomTextField!

    @IBOutlet weak var hoursTagLbl: UILabel!
    @IBOutlet weak var hoursLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let calcBtn = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60))
        calcBtn.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.4920324053, blue: 0.07256629024, alpha: 1)
        calcBtn.setTitle("Calculate", for: .normal)
        calcBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        calcBtn.addTarget(self, action: #selector(MainViewController.calculate), for: .touchUpInside)
        
        wageTxt.inputAccessoryView = calcBtn
        priceTxt.inputAccessoryView = calcBtn
        
        hoursTagLbl.isHidden = true
        hoursLbl.isHidden = true
    }

    @objc func calculate() {
        
        view.endEditing(true)
        
        if let wageTxt = wageTxt.text, let priceTxt = priceTxt.text {
            if let wage = Double(wageTxt), let price = Double(priceTxt) {
                hoursLbl.text = "\(Wage.getHours(ForWage: wage, AndPrice: price))"
            }
        }
        
        hoursTagLbl.isHidden = false
        hoursLbl.isHidden = false
    }

    @IBAction func clearCalcBtnPressed(_ sender: Any) {
        hoursTagLbl.isHidden = true
        hoursLbl.isHidden = true
        wageTxt.text = ""
        priceTxt.text = ""
    }
    
}

